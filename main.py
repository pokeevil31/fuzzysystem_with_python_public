# import fuzzy_sys
import chikuji
# import math
import numpy as np
from matplotlib import pyplot

from fuzzy_sys import Fuzzyset, Fuzzysystem

# a = [0]
# s = fuzzy_sys.Fuzzysystem()

# a[0] = fuzzy_sys.Fuzzyset(0.0, 1.0, 2.0)
# r = fuzzy_sys.Fuzzyrule(1, a, 1.0)
# s.add_rule(r)

# a[0] = fuzzy_sys.Fuzzyset(1.0, 2.0, 3.0)
# r = fuzzy_sys.Fuzzyrule(1, a, 3.0)
# s.add_rule(r)

# a[0] = fuzzy_sys.Fuzzyset(2.0, 3.0, 4.0)
# r = fuzzy_sys.Fuzzyrule(1, a, 0.5)
# s.add_rule(r)

# s.print_sys()

# input = [0]
# x = []
# y = []
# for i in range(21):
#     input[0] = 1.0 + i/10.0
#     x.append(input[0])
#     result = s.fuzzyreasoning(input)
#     y.append(result)
#     print('%f %f' % (input[0], result))

# x = np.array(x)
# y = np.array(y)
# pyplot.plot(x, y)
# pyplot.show()

xt = chikuji.creat_data(-2*np.pi, 2*np.pi, 100)
yt = chikuji.data_func(xt)
input = chikuji.change_to_input(xt)
print(yt)

# s = chikuji.chikuji(input, yt)
# s.write_file()

s = Fuzzysystem()
s.read_file()

result = []
for i in input:
    result.append(s.fuzzyreasoning(i))

xt = np.array(xt)
yt = np.array(yt)

x = np.linspace(-2*np.pi, 2*np.pi, 101)
# y = np.sin(x)
y = x**2
pyplot.plot(x, y, "r", xt, yt, "ob")
pyplot.show()
