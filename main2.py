import time
# import fuzzy_sys
import chikuji
# import math
import numpy as np
from matplotlib import pyplot

from fuzzy_sys import Fuzzyset, Fuzzysystem
from mpl_toolkits.mplot3d import Axes3D

startTime = time.time()

xt, yt = chikuji.creat_data(-10, 10, 100, 2)
# xt = np.array(xt)
# yt = np.array(yt)
zt = chikuji.data_func2(xt, yt)
input = chikuji.change_to_input(xt, yt)

s = Fuzzysystem()
# s.read_file("annealing_test_0.95.json")
s.read_file("annealing_test_0.95+50.json")
# s.read_file("annealing_test_0.90.json")
# s.read_file("annealing_test_0.80.json")

# s = chikuji.chikuji(input, zt)
# s.write_file("annealing_test_1.json")
# s.print_sys()


# # plot error changing
# s = Fuzzysystem()
# s.read_file()
# n = len(input)
# tel = []    # total error list
# rnl = []    # rules number list
# for j in range(s.rulenum):
#     rnl.append(j)
#     te = 0
#     for i in range(n):
#         te += abs(s.fuzzyreasoning(input[i], j) - yt[i])
#     tel.append(te)
# pyplot.plot(rnl, tel)


# result = []
# for i in input:
#     rp = s.fuzzyreasoning(i)
#     # print(rp)
#     result.append(rp)


result = []
for j in yt:
    tmp = []
    for i in xt:
        tmp.append(s.fuzzyreasoning([i, j]))
    result.append(tmp)
result = np.array(result)

x = np.linspace(-10, 10, 101)
y = x
x, y = np.meshgrid(x, y)
z = x**2 + y**2
# z = np.sin(x) + np.cos(y)

ax = pyplot.subplot(111, projection='3d')
ax.plot_wireframe(x, y, result, color='red', linewidth=0.3)
ax.plot_wireframe(x, y, z, color='blue', linewidth=0.4)

endTime = time.time()
print("Running Time: %f (s)" % (endTime - startTime))

pyplot.show()
