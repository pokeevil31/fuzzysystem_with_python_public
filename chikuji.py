from fuzzy_sys import Fuzzyrule as fr, Fuzzyset as fse, Fuzzysystem as fsy
import math
import time
import random


def total_error(s, x, y):
    n = len(x)
    te = 0
    for i in range(n):
        te += abs(s.fuzzyreasoning(x[i]) - y[i])
    return te


def total_error_random(s, x, y):
    n = len(x)
    te = 0
    for i in range(1000):
        rand = math.floor(random.uniform(0, n))
        if rand >= n:
            rand = n-1
        te += abs(s.fuzzyreasoning(x[rand]) - y[rand])
    return te


# s: fuzzy system, x: input list, y: output list
def chikuji(x, y, s=None):
    print("Start Chikuji.")
    start = time.time()
    rn = 50         # max rules
    n = len(x)      # data num
    m = len(x[0])   # input dim
    e = 0           # reasoning error
    a = [0]         # fuzzy rule
    t = 100         # searching times
    dim = len(x[0])
    print("Dim = %d" % dim)

    # # total error list
    # tel = [total_error(s, x, y)]

    # get y_max and y_min
    y_max = -float('inf')
    y_min = float('inf')
    for i in range(len(y)):
        if(y_max < y[i]):
            y_max = y[i]
        if(y_min > y[i]):
            y_min = y[i]

    # get x_max, x_min and width of x
    x_max = [-float('inf')] * dim
    x_min = [float('inf')] * dim
    for i in range(dim):
        for j in x:
            if(x_max[i] < j[i]):
                x_max[i] = j[i]
            if(x_min[i] > j[i]):
                x_min[i] = j[i]
    xw = [x_max[i]-x_min[i] for i in range(dim)]    # width of x

    # initial system
    if s is None:
        s = fsy()
        a = [fse(x_min[i]-1, (x_max[i]+x_min[i])/2, x_max[i]+1) for i in range(dim)]
        te = float("inf")
        kn = 0.0
        for i in range(t+1):
            ko = (y_min - (y_max - y_min)) + i*3*(y_max - y_min)/t
            tmp = total_error(fsy(fr(dim, a, ko)), x, y)
            if(te > tmp):
                te = tmp
                kn = ko
        s.add_rule(fr(dim, a, kn))

    si = fsy()                  # si： backup of s

    # annealing liked
    temp = 0.5
    dr = 0.98
    rc = s.rulenum-1
    while rc > 0 and temp > 0.01:
        temp *= dr
        rc -= 1

    for i in range(1, rn+1):
        if(temp > 0.01):
            temp *= dr

        # find center of new set
        e = 0                   # reasoning error
        # for j in range(n):      # without random
        #     tmp = abs(s.fuzzyreasoning(x[j]) - y[j])
        #     if(e < tmp):
        #         cen = x[j]
        #         e = tmp
        for j in range(1000):
            randx = [random.uniform(x_min[k], x_max[k]) for k in range(dim)]
            tmp = abs(s.fuzzyreasoning(randx) - y[j])
            if(e < tmp):
                cen = randx
                e = tmp
        # new set
        # a = [fse(nc-1, nc, nc+1) for nc in cen]
        a = [fse(cen[j]-xw[j]*temp, cen[j], cen[j]+xw[j]*temp) for j in range(dim)]
        # print('Center %f' % (cen), end=" \t")
        nr = fr(m, a, 0)                # new rule
        kn = 0                          # new koken
        count = 0
        si.copy_sys(s)                  # si： backup of s

        te = float("inf")               # total error
        # find new kokenbu
        for j in range(t+1):
            # ko = y_min + j*(y_max - y_min)/t    # searching koken
            ko = (y_min - (y_max - y_min)) + j*3*(y_max - y_min)/t
            nr.set_koken(ko)
            # print("Searching: %f\tnr_koken: %f" % (ko, nr.koken))
            # si = s              # si： backup of s

            s.add_rule(nr)
            # print("s:%d\tsi:%d" % (len(s.fr), len(si.fr)))
            count += 1
            # compare the total error
            # tmp = total_error(s, x, y)
            tmp = total_error_random(s, x, y)
            if(te > tmp):
                kn = ko
                te = tmp
            s.copy_sys(si)      # recover s to si

        # tel.append(te)

        # if(te > total_error(si, x, y)):  # total error should be smaller
        #     print("Total error cannot be smaller, stop adding rule.")
        #     break

        # add new rule
        nr.set_koken(kn)
        s.add_rule(nr)
        print('Add Rule %d' % (i))
        # print("Loop times: %d" % (count))
        # print("Rules number: %d" % (len(s.fr)))

    # print("Total add_rule: %d" % (count))
    end = time.time()
    print("Chikuji Fuzzy Modeling Over. Time>>> %f" % (end - start))
    return s


def creat_data(start, stop, num, dim=1):
    x = []
    if(dim >= 2):
        y = []
        if(dim >= 3):
            z = []

    for i in range(num+1):
        tmp = start + i * (stop-start) / num
        x.append(tmp)
        if(dim >= 2):
            y.append(tmp)
        if(dim >= 3):
            z.append(tmp)

    if(dim == 2):
        return x, y
    if(dim == 3):
        return x, y, z
    return x


# chanage a list to a list of lists
def change_to_input(x, y=None, z=None):
    input = []

    if(y is None):
        for i in range(len(x)):
            input.append([x[i]])
    elif(z is None):
        for i in range(len(x)):
            for j in range(len(y)):
                input.append([x[i], y[j]])
    else:
        for i in range(len(x)):
            for j in range(len(y)):
                for k in range(len(z)):
                    input.append([x[i], y[j], z[k]])

    return input


def data_func(x):
    y = []

    for i in x:
        y.append(math.sin(i))
        # y.append(i**2)

    return y


def data_func2(x, y):
    z = []

    for i in range(len(x)):
        for j in range(len(x)):
            tmp = x[i]**2 + y[i]**2
            z.append(tmp)

    return z


def total_error_changing(s, input, output):
    n = len(input)
    tel = []
    # rnl = []
    for j in range(s.rulenum):
        # rnl.append(j)
        te = 0
        for i in range(n):
            te += abs(s.fuzzyreasoning(input[i], j) - output[i])
        tel.append(te)
    return tel
