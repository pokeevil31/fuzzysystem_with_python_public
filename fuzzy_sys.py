from os import close
import json
import math
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from numpy import int16


class Point:
    def __init__(self, x, g):
        self.x = x
        self.g = g


# class Point2:
#     def __init__(self, x, y, g):
#         self.x = x
#         self.y = y
#         self.g = g


class Fuzzyset:
    # point_lim = 10

    def __init__(self, m1, m2, m3):
        self.pointnum = 0
        self.p = []
        self.triangle(m1, m2, m3)
        self.trip = [m1, m2, m3]    # used for json

    def add_point(self, x, g):
        # if(self.pointnum >= self.point_lim):
        #     print('Error : Points overflow.')
        #     return
        self.p.append(Point(x, g))
        self.pointnum += 1

    def grade(self, x):
        i = 0
        while(1):
            if(i >= self.pointnum):
                break
            if(x < self.p[i].x):
                break
            i += 1

        if(i <= 0):
            return 0.0
        if(i >= self.pointnum):
            return 0.0

        ax = self.p[i-1].x
        ag = self.p[i-1].g
        bx = self.p[i].x
        bg = self.p[i].g
        g = (bg - ag) * (x - bx) / (bx - ax) + bg
        return g

    def triangle(self, m1, m2, m3):
        self.add_point(m1, 0.0)
        self.add_point(m2, 1.0)
        self.add_point(m3, 0.0)

    def print_tri(self):
        # print(self.pointnum)
        print('(', end='')
        i = 0
        while(self.pointnum):
            print(self.p[i].x, end='')
            i += 1
            if(i >= self.pointnum):
                break
            print(',', end=' ')
        print(')', end='')


class Fuzzyrule:
    zenken_lim = 10

    def __init__(self, num, a, koken):
        self.zenken_num = num
        self.zenken = []
        self.set_zenken(a)      # a: list of fuzzy set
        self.koken = koken

    # fset: Fuzzyset
    def set_zenken(self, fset):
        for i in range(self.zenken_num):
            self.zenken.append(fset[i])

    def set_koken(self, koken):
        self.koken = koken

    def match(self, x):
        dim = self.zenken_num
        match = 1.0

        for i in range(dim):
            g = self.zenken[i].grade(x[i])
            match = min(match, g)

        return match

    def print_rule(self):
        # print(self.zenken_num)
        print('[', end='')
        i = 0
        while(self.zenken_num):
            self.zenken[i].print_tri()
            i += 1
            if(i >= self.zenken_num):
                break
            print(' + ', end='')
        print('] -> %f' % self.koken)


class Fuzzysystem:
    # rule_lim = 50

    def __init__(self, ir=None):
        self.rulenum = 0
        self.fr = []
        self.te = [float("inf")]
        if ir is not None:
            self.add_rule(ir)

    def add_rule(self, r):
        # if(self.rulenum >= self.rule_lim):
        #     print('Error : Fuzzy rules overflow.')
        #     return
        self.fr.append(r)
        self.rulenum += 1

    def fuzzyreasoning(self, x, num=-1):
        if(num == -1):
            num = self.rulenum
        nu = de = 0.0
        for i in range(num):
            e = self.fr[i].match(x)
            nu += e * self.fr[i].koken
            de += e

        if(de == 0.0):
            # print('Error : Fuzzy reasoning : underflow')
            return 0.0

        result = nu / de
        return result

    def print_sys(self):
        print('fr_num = %d' % (self.rulenum))
        for i in range(self.rulenum):
            self.fr[i].print_rule()

    def init_sys(self):
        self.rulenum = 0
        self.fr = []
        self.te = [float("inf")]

    def copy_sys(self, sc):
        self.init_sys()
        for i in sc.fr:
            self.add_rule(i)

    def write_file(self, fname="fuzzy_system.json"):
        f = open(fname, 'w+')
        sys = {'Rules': []}
        count = 0
        for i in self.fr:
            # print(count)
            rule = {'No.': count, 'zenken': [], 'koken': 0.0}
            count += 1
            for j in i.zenken:
                rule['zenken'].append(j.trip)
            rule['koken'] = i.koken
            sys['Rules'].append(rule)
        json.dump(sys, f, indent=4)
        f.close()

    def read_file(self, fname="fuzzy_system_2.json"):
        f = open(fname, 'r+')
        sys = json.load(f)
        sys = sys['Rules']
        for i in sys:
            fset = []
            for j in i['zenken']:
                fset.append(Fuzzyset(j[0], j[1], j[2]))
            rule = Fuzzyrule(len(fset), fset, i['koken'])
            self.add_rule(rule)
        f.close()

    def show_zenken_range2(self):
        # fig = plt.figure()
        cl = ['red', 'hotpink', 'yellow', 'limegreen', 'deepskyblue', 'orange']
        ax = plt.axes()
        count = 0
        for i in self.fr:
            x = i.zenken[0]
            y = i.zenken[1]
            w = x.trip[2] - x.trip[0]
            h = y.trip[2] - y.trip[0]
            # print("%6f, %6f, %6f, %6f" % (x.trip[0], y.trip[0], w, h))
            tmp = patches.Rectangle(xy=(x.trip[0], y.trip[0]), width=w, height=h, ec=cl[math.floor(count/9)], fill=False)
            ax.add_patch(tmp)
            count += 1
            # print('count = %d' % (count))
        plt.axis('scaled')
        plt.show()
